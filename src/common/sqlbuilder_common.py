from sqlbuilder.smartsql import (T, F, Q, Value, Cast, func, Constant,
                                 TableJoin, compile)  # noqa
from functools import partial


def extr_path(target, *args, as_type=None, extractor=func.jsonb_extract_path):
    e = extractor(target, *[V(arg) for arg in args])
    return Cast(e, as_type) if as_type else e


dig, dig_t, V = extr_path, partial(extr_path, extractor=func.jsonb_extract_path_text), Value

DOUBLE, BOOLEAN, INTEGER, TEXT = 'double precision', 'boolean', 'bigint', 'text'

# [NGibaev 30.08.18] use only for upserts
EXCLUDED_T = T.excluded
