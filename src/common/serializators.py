from datetime import datetime


def as_is(v):
    return v


def as_lang_dict(v):
    if isinstance(v, str):
        fake_russian_name = {'ru': v}
        return fake_russian_name
    return v


def as_datetime(v: datetime) -> str:
    if v:
        return v.isoformat()
