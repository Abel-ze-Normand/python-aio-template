import logging
from typing import List


__all__ = [
    'ServiceResult', 'Service', 'guard_service_call',
    'ServiceFailException', 'ServicesLinkedPipeline'
]


logger = logging.getLogger(__name__)


class ServiceFailException(Exception):
    """
    Interruption for current service but your passed
    value would be passed to next service in pipeline
    """

    def __init__(self, error_val_args: List[object]=None, error_val_kw: dict=None, msg=None):
        self.error_val_args = error_val_args
        self.error_val_kw = error_val_kw
        msg = msg or f"Recovering with error_val ({error_val_args} | {error_val_kw})"
        super().__init__(msg)


def guard_service_call(func, is_allow_panic=True):
    """
    set flag ``is_allow_panic'' to false only if you want to manually check services completion and
    suppress theirs errors. keep in mind that if set this flag to true for service call which used
    in pipeline, there can be unexpected result!
    """

    async def func_wrapper(*args, **kwargs):
        try:
            return await func(*args, **kwargs)
        except ServiceFailException as e:
            return ServiceResult(error_val_args=e.error_val_args, error_val_kw=e.error_val_kw)
        except Exception as e:
            if is_allow_panic:
                raise e
            else:
                return ServiceResult(error_reason=str(e))

    return func_wrapper


class ServiceResult:
    result_args: List[object] = None
    result_kw: dict = None
    error_reason: str = None
    error_val_args: List[object] = ()
    error_val_kw: dict = {}

    def __init__(self, result_args=None, result_kw=None, error_reason: str = None,
                 error_val_args: List[object] = None, error_val_kw: dict = None):
        self.result_args = result_args or []
        self.result_kw = result_kw or {}
        self.error_reason = error_reason
        self.error_val_args = error_val_args or []
        self.error_val_kw = error_val_kw or {}

    @property
    def is_need_recover(self) -> bool:
        return self.error_val_args or self.error_val_kw

    @property
    def is_successful(self) -> bool:
        return self.error_reason is None and self.error_val is None

    def __repr__(self):
        return f"<ServiceResult: [result: {self.result_args}, {self.result_kw}] | [error_reason: {self.error_reason}] | [error_val: {self.error_val_args}, {self.error_val_kw}]>"


class Service:
    def __init__(self, *args, **kwargs):
        self.__dict__.update(kwargs)

    async def call(self) -> ServiceResult:
        raise NotImplementedError()

    async def recover(self) -> ServiceResult:
        raise NotImplementedError()


class ServiceWithArgsCurrying(Service):
    def __init__(self, service_cls, *args, **kwargs):
        self.service_cls = service_cls
        self.curry_args = args
        self.curry_kwargs = kwargs

    def __call__(self, *args, **kwargs):
        return self.service_cls(*self.curry_args, *args, **self.curry_kwargs, **kwargs)

    def __repr__(self):
        return f"<CURRY -> {self.curry_args} | {self.curry_kwargs} to {self.service_cls}>"


class ServicesLinkedPipeline:
    def __init__(self, *args, **kwargs):
        """
        :params:

        :args: Sequence of services
        """
        self.services = args

    async def emit(self, *args, **kwargs):
        """
        :params:

        args and kwargs would be passed to first service
        """
        # start with fake service result (we assume that first service in sequence always starts with ``call``)
        first = self.services[0]
        curr: ServiceResult = await first(*args, **kwargs).call()
        for s in self.services[1:]:
            logger.debug(f'PIPELINE: step into {s} with result {curr}')
            if curr.is_need_recover:
                curr = await s(*curr.error_val_args, **curr.result_kw).recover()
            else:
                curr = await s(*curr.result_args, **curr.result_kw).call()
        logger.debug(f'PIPELINE: end pipeline with result {curr}')
        return curr
