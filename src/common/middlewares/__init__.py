from .log_middleware import log_middleware


def get_middlewares():
    list_middlewares = (log_middleware,)
    return list_middlewares
