import asyncio
from aiohttp.web_exceptions import HTTPNotFound, HTTPBadRequest
import sys
from common.utils.exceptions import HTTPBadRequest as OurHTTPBadRequest
from common.utils.logger import app_logger
from common.utils.http_tools import return_json, serialize_exception


async def log_middleware(app, handler):
    async def middleware_handler(request):
        r_id = request.headers.get('X-Request-Id', '')
        app_logger.debug(f'{r_id} :: {request.rel_url}')
        try:
            response = await handler(request)
        except asyncio.CancelledError:
            pass
        except (HTTPNotFound, HTTPBadRequest, OurHTTPBadRequest) as e:
            error_info = serialize_exception(e, r_id=r_id)
            response_status = e.status_code
            return return_json(error_info, response_status)
        except Exception as e:
            app_logger.exception(f'{r_id} :: {e} in {handler.__module__}:{handler.__name__}')
            error_info = serialize_exception(e, handler.__module__.split(".")[-2:][0], r_id)
            response_status = error_info["error"]["code"]
            return return_json(error_info, response_status)
        except:
            app_logger.exception(f"Unexpected error in {r_id}: {sys.exc_info()[0]}")
        else:
            response_status = response.status
            return response
        finally:
            app_logger.info(f'{r_id} :: {request.rel_url} :: {response_status}')
    return middleware_handler
