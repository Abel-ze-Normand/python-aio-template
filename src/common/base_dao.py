from functools import partial
import ujson
from common.utils.utils import change_dict_naming_convention
from asyncpg.pool import Pool
from common.utils.exceptions import PaginationOverflow


class AbstractDAO:
    @property
    def has_collect(self):
        return bool(self.collect_spec)

    @property
    def has_save(self):
        return bool(self.save_spec)

    @property
    def has_update(self):
        return bool(self.update_spec)

    @property
    def has_delete(self):
        return bool(self.delete_spec)


class BaseSnakeCaseCursorCopyDAO(AbstractDAO):
    collect_spec = None
    """
    Base DAO class for implementing streaming data via cursor into external sources
    """

    def __init__(self, pool: Pool, response, **_):
        self._pool = pool
        self.response = response

    async def _copy_from_query(self, q, args, callback: callable, **_):
        """
        Iterate over each query result and execute callback over each row
        """
        async with self._pool.acquire() as con:
            async with con.transaction():
                async for r in con.cursor(q, *args):
                    await callback(r)

    async def _write_to_stream_response(self, response, row: dict):
        row_raw = bytearray(row['row'], encoding='utf8') + b'\n'
        await response.write(row_raw)

    async def collect(self, data):
        q, args = self.collect_spec(**data).collect_q_str
        await self._copy_from_query(q, args,
                                    partial(self._write_to_stream_response, self.response))


class BaseCamelCaseCursorCopyDAO(AbstractDAO):
    """
    Base DAO class for implementing streaming data via cursor into external sources
    """

    def __init__(self, pool: Pool, response, **_):
        self._pool = pool
        self.response = response

    async def _copy_from_query(self, q, args, callback: callable, **_):
        """
        Iterate over each query result and execute callback over each row
        """
        async with self._pool.acquire() as con:
            async with con.transaction():
                async for r in con.cursor(q, *args):
                    await callback(r)

    async def _write_to_stream_response(self, response, row: dict):
        camelcased_row = ujson.dumps(change_dict_naming_convention(row['row']))
        row_raw = bytearray(camelcased_row, encoding='utf8') + b'\n'
        await response.write(row_raw)

    async def collect(self, data):
        q, args = self.collect_spec(**data).collect_q_str
        await self._copy_from_query(q, args,
                                    partial(self._write_to_stream_response, self.response))


class AdapterDbQuery:
    def __init__(self, pool: Pool, **_):
        self._pool = pool

    async def fetch(self, query: str, *args, timeout: int=None, as_dicts: bool=False) -> list:
        """
        Исполнить запрос и вернуть данные
        :param str query: запрос
        :param args: параметры запроса
        :param float timeout: таймаут в секундах
        :param bool as_dicts: вернуть как список словарей
        :return: список из :class:`Record` или словарей
                 или None если запрос ничего не вернул
        """
        records = await self._pool.fetch(query, *args, timeout=timeout)
        if records and as_dicts:
            return [{k: v for k, v in record.items()} for record in records]
        return records

    async def fetchval(self, query: str, *args, column: int=0, timeout: int=None):
        """
        Исполнить запрос и вернуть значение колонки из первой строки
        :param str query: запрос
        :param args: параметры запроса
        :param int column: номер колонки из которой взять значение
        :param float timeout: таймаут в секундах
        :return: :class:`Record` или словарь
                 или None если запрос ничего не вернул
        """
        return await self._pool.fetchval(query, *args, column=column,
                                         timeout=timeout)

    async def fetchrow(self, query: str, *args, timeout: int=None, as_dicts: bool=False):
        """
        Исполнить запрос и вернуть данные первой строки
        :param str query: запрос
        :param args: параметры запроса
        :param float timeout: таймаут в секундах
        :param bool as_dicts: вернуть как список словарей
        :return: :class:`Record` или словарь
                 или None если запрос ничего не вернул
        """
        record = await self._pool.fetchrow(query, *args, timeout=timeout)
        if record and as_dicts:
            return {k: v for k, v in record.items()}
        return record


class BaseDAO(AbstractDAO):
    _fields: dict = {}
    _exclude_fields: tuple = ()
    collect_spec: callable = None
    save_spec: callable = None
    save_resp_spec: callable = None
    update_spec: callable = None
    update_resp_spec: callable = None
    delete_spec: callable = None
    delete_resp_spec: callable = None

    def __init__(self, pool: Pool, **_):
        self._pool = pool

    def _prepare_params(self, data: dict):
        self.data = data
        fields = self.data.get('fields', None)
        self.data.update({'fields': fields})

    @classmethod
    def available_fields(cls) -> (str, list):
        return tuple(cls._fields.keys())

    def _serialize_field(self, field_name: str, value: str) -> object:
        v = self._fields[field_name](value)
        return v

    def _prepare_items(self, items: list, data: dict) -> list:
        item_list = [{field: self._serialize_field(field, value)
                      for field, value in item.items()
                      if field in self._fields}
                     for item in items]
        return self.patch_items(item_list, data)

    async def collect(self, data: dict) -> dict:
        """
        Public method for get data from db with sql adapter rules
        :param data: data which influence for get data in sql/specs
        :return: Object with needed data
        """
        self._prepare_params(data)
        spec = self.collect_spec(**data)
        sql, params = spec.collect_q_str
        items = await AdapterDbQuery(self._pool).fetch(sql, *params)
        sql, params = spec.total_q_str
        total = await AdapterDbQuery(self._pool).fetchval(sql, *params)
        offset = self.data.get("offset")
        if total >= 0 and offset and offset >= total:
            raise PaginationOverflow(offset, total)
        return {
            'total': total,
            'items': self._prepare_items(items, data),
        }

    async def save(self, data: dict, *args, **kwargs) -> dict:
        self._prepare_params(data)
        spec = self.save_spec(data, *args, **kwargs)
        sql, params = spec.save_q_str
        save = await AdapterDbQuery(self._pool).fetch(sql, *params)
        return save
        # [NGibaev 31.08.18] commented for now: dont know how to handle returned key (if exists)
        # data.update(save)
        # if self.update_resp_spec:
        #     save_spec = self.save_resp_spec(**data)
        #     return {
        #         'items': await AdapterDbQuery(self._pool).fetch(*save_spec.save_object_q_str)
        #     }
        # return await self.collect(data)

    async def update(self, data: dict, *args, **kwargs) -> dict:
        self._prepare_params(data)
        spec = self.update_spec(data, *args, **kwargs)
        sql, params = spec.update_q_str
        update = await AdapterDbQuery(self._pool).fetch(sql, *params)
        return update
        # [NGibaev 31.08.18] commented for now: dont know how to handle returned key (if exists)
        # data.update(update)
        # if self.update_resp_spec:
        #     update_spec = self.update_resp_spec(**data)
        #     return {
        #         'items': await AdapterDbQuery(self._pool).fetch(*update_spec.save_object_q_str)
        #     }
        # return await self.collect(data)

    async def delete(self, data: dict, *args, **kwargs) -> dict:
        self._prepare_params(data)
        spec = self.delete_spec(data, *args, **kwargs)
        sql, params = spec.delete_q_str
        delete = await AdapterDbQuery(self._pool).fetch(sql, *params)
        return delete
        # [NGibaev 31.08.18] commented for now: dont know how to handle returned key (if exists)
        # if self.update_resp_spec:
        #     delete_spec = self.delete_resp_spec(**data)
        #     response = {
        #         'items': await AdapterDbQuery(self._pool).fetch(*delete_spec.save_object_q_str)
        #     }
        # else:
        #     response = await self.collect(data)
        # await AdapterDbQuery(self._pool).fetch(*spec.delete_q_str)
        # return response

    def patch_items(self, items: list, data: dict) -> list:
        return items
