import time
import asyncio

import prometheus_client
from aiohttp import web


__all__ = ['setup_metrics']


def prom_middleware(app_name):
    async def factory(app, handler):
        async def middleware_handler(request):
            try:
                request['start_time'] = time.time()
                
                request.app['REQUEST_IN_PROGRESS'].labels(
                    app_name,
                    request.path,
                    request.method
                ).inc()
                
                response = await handler(request)
                
                resp_time = time.time() - request['start_time']
                request.app['REQUEST_LATENCY'].labels(
                    app_name,
                    request.path
                ).observe(resp_time)
                
                request.app['REQUEST_IN_PROGRESS'].labels(
                    app_name,
                    request.path,
                    request.method
                ).dec()
                
                request.app['REQUEST_COUNT'].labels(
                    app_name,
                    request.method,
                    request.path,
                    response.status
                ).inc()
                
                return response
            except asyncio.CancelledError:
                pass
            except Exception as ex:
                raise
        return middleware_handler
    return factory


async def metrics(request):
    resp = web.Response(body=prometheus_client.generate_latest())
    resp.content_type = prometheus_client.exposition.CONTENT_TYPE_LATEST
    return resp


def setup_metrics(app, app_name):
    app['REQUEST_COUNT'] = prometheus_client.Counter(
      'requests_total', 'Total Request Count',
      ['app_name', 'method', 'endpoint', 'http_status']
    )
    
    app['REQUEST_LATENCY'] = prometheus_client.Histogram(
        'request_latency_seconds', 'Request latency',
        ['app_name', 'endpoint']
    )

    app['REQUEST_IN_PROGRESS'] = prometheus_client.Gauge(
        'requests_in_progress_total', 'Requests in progress',
        ['app_name', 'endpoint', 'method']
    )

    app.middlewares.insert(0, prom_middleware(app_name))
    app.router.add_get("/metrics", metrics, name='prometheus_metrics')
