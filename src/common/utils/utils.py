import inflection
from typing import Dict, Union, Callable

Field = Union[str, int]


class DictWithoutNoneValue(dict):
    def __setitem__(self, key, value):
        if key in self or value is not None:
            dict.__setitem__(self, key, value)


def camel_to_underscore(name):
    return inflection.underscore(name)


def underscore_to_camel(name):
    return inflection.camelize(name, False)


def change_dict_naming_convention(d: Union[Dict, Field],
                                  convert_function: Callable=underscore_to_camel) -> Union[Dict, Field]:
    new = {}
    if isinstance(d, int) or d is None:
        return d
    if isinstance(d, str):
        return convert_function(d)
    for k, v in d.items():
        new_v = v
        if isinstance(v, dict):
            new_v = change_dict_naming_convention(v, convert_function)
        elif isinstance(v, list):
            new_v = list()
            for x in v:
                new_v.append(change_dict_naming_convention(x, convert_function))
        new[convert_function(k)] = new_v
    return new
