from asyncio import CancelledError
from typing import Optional
import aiohttp
import ujson
from aiohttp import hdrs
import logging
from .exceptions import HttpException

app_logger = logging.getLogger('services')


async def get_decoded_response(response):
    app_logger.debug('Decoding response...')
    try:
        data = await response.json(loads=ujson.loads)
    except Exception as e:
        app_logger.exception('Decoding response failed')
        err = HttpException(message='Decoding response failed', reason=e.__class__.__name__)
        return err, False
    else:
        app_logger.debug('Decoding response is done')
        return data, True


async def api_request(method: str, url: str,
                      params: str=None,
                      data: Optional[str]=None,
                      json: Optional[str]=None,
                      headers=None,
                      skip_auto_headers=None,
                      auth: dict=None,
                      allow_redirects: bool=True,
                      max_redirects: int=10,
                      compress=None,
                      chunked=None,
                      expect100: bool=False,
                      read_until_eof: bool=True,
                      proxy: str=None,
                      proxy_auth: Optional[str]=None,
                      conn_timeout: int=5,
                      read_timeout: int=60,
                      json_serialize=ujson.loads,
                      raise_for_status: bool=True,
                      service_name: str=None):
    try:
        session_kwargs = dict(
            conn_timeout=conn_timeout,
            read_timeout=read_timeout,
            headers=headers,
            json_serialize=json_serialize,
            raise_for_status=raise_for_status,
        )
        kwargs = dict(
            params=params,
            data=data,
            json=json,
            skip_auto_headers=skip_auto_headers,
            auth=auth,
            allow_redirects=allow_redirects,
            max_redirects=max_redirects,
            compress=compress,
            chunked=chunked,
            expect100=expect100,
            read_until_eof=read_until_eof,
            proxy=proxy,
            proxy_auth=proxy_auth,
        )
        app_logger.debug('Trying [%s] %s with %s and %s',
                         method, url, kwargs, session_kwargs)

        async with aiohttp.ClientSession(**session_kwargs) as session:
            async with session.request(method, url, **kwargs) as response:
                return await get_decoded_response(response)
    except CancelledError:
        return
    except Exception as e:
        app_logger.exception(f'[{method}] {url} failed')
        err = HttpException(domain=service_name, message=f'[{method}] {url} failed',
                            reason=e.__class__.__name__, location=method, location_type='api')
        return err, False


class BaseExternalService:
    url: str = None
    auth: dict = None
    skip_auto_headers: str = None
    max_redirects: int = 10
    allow_redirects: bool = True
    expect100: bool = False
    read_until_eof: bool = True
    json_serialize = ujson.loads
    raise_for_status: bool = True
    proxy: str = None
    proxy_auth: Optional[str] = None
    conn_timeout: int = None
    read_timeout: int = None

    def __init__(self, headers):
        self.headers = headers

    async def call(self, params):
        raise NotImplementedError

    async def _api_get(self, params: Optional[str]=None, data: Optional[str]=None, json: Optional[str]=None,
                       compress: str=None, chunked: str=None):
        return await api_request(hdrs.METH_GET, self.url, params, data, json, self.headers,
                                 self.skip_auto_headers, self.auth, self.allow_redirects,
                                 self.max_redirects, compress, chunked,
                                 self.expect100, self.read_until_eof, self.proxy, self.proxy_auth,
                                 self.conn_timeout, self.read_timeout, self.json_serialize,
                                 self.raise_for_status)

    async def _api_post(self, params: Optional[str]=None, data: Optional[str]=None, json: Optional[str]=None,
                        compress: str=None, chunked: str=None):
        return await api_request(hdrs.METH_POST, self.url, params, data, json, self.headers,
                                 self.skip_auto_headers, self.auth, self.allow_redirects,
                                 self.max_redirects, compress, chunked,
                                 self.expect100, self.read_until_eof, self.proxy, self.proxy_auth,
                                 self.conn_timeout, self.read_timeout, self.json_serialize,
                                 self.raise_for_status)
