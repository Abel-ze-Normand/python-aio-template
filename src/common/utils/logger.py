import logging
from conf import settings
from logging.config import dictConfig
from raven_aiohttp import AioHttpTransport

LOG_DICT = {
    'version': 1,
    'disable_existing_loggers': True,

    'formatters': {
        'default': {
            'format': '{\"logtime\": \"%(asctime)s\", \"loglevel\": \"%(levelname)s\", \"logger\": \"%(name)s\", '
                      '\"extra\": {\"filename\": \"%(filename)s\", '
                      '\"funcName\": \"%(funcName)s\", \"lineno\": %(lineno)d, \"logmessage\": \"%(message)s\"}}',
        },
    },

    'handlers': {
        'console': {
            'level': settings.LOG_LEVEL,
            'class': 'logging.StreamHandler',
            'formatter': 'default'
        },
        'sentry': {
            'class': 'raven.handlers.logging.SentryHandler',
            'level': settings.SENTRY_LEVEL,
            'dsn': settings.SENTRY_DSN,
            'transport': AioHttpTransport,
            'formatter': 'default',
            'auto_log_stacks': True
        },
    },

    'loggers': {
        'main': {
            'handlers': ('console', 'sentry'),
            'level': 'DEBUG',
        },
        'apps': {
            'handlers': ('console', 'sentry'),
            'level': 'DEBUG',
        },
        'common': {
            'handlers': ('console', 'sentry'),
            'level': 'DEBUG',
        },
        'api': {
            'handlers': ('console', 'sentry'),
            'level': 'DEBUG',
        },
        'services': {
            'handlers': ('console', 'sentry'),
            'level': 'DEBUG',
        },
        'conf.db': {
            'handlers': ('console', 'sentry'),
            'level': 'DEBUG',
        },
    }
}


dictConfig(LOG_DICT)
app_logger = logging.getLogger('main')
