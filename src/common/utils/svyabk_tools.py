import re
from .exceptions import HttpException


class SvyabkValidationException(HttpException):
    status_code = 400


# [NGibaev 30.08.18] REMOVEME deprecated use for microservices
# def svyabk_to_dict(svyabk: str=''):
#     separated = svyabk.split('-')
#     if len(separated) == 5:
#         object_names = ['country_id', 'currency_id', 'language_id', 'business_id', 'channel_id']
#         return {object_names[num]: item for num, item in enumerate(separated)}
#     return {}


def svyabk_wildcards(svyabk: str='', mask: list=None):
    """
    маскиратор свябка
    :param svyabk:
    :param mask: [1,0,0,1,0] (1 - включить, 0 - звёздочка)
    :return:
    """
    separated = svyabk.split('|')
    if len(separated) == len(mask):
        return '-'.join([item if mask[num] else '*' for num, item in enumerate(separated)])
    return '*-*-*-*-*'


def valid_svyabk(svyabk: str):
    SVYABK_RE_STR = r'^(([A-Z]+|\*)-){2}([a-z]+|\*)-([A-Z]+|\*)-[^\-]*$'
    SVYABK_RE_COMP = re.compile(SVYABK_RE_STR)
    if not svyabk:
        raise SvyabkValidationException(reason='no svyabk supplied', message='Свябк не потдерживается')
    if not re.match(SVYABK_RE_COMP, svyabk):
        raise SvyabkValidationException(reason='Bad svyabk format', message='Не правильный формат свябка')


# def get_original_svyabk_object(svyabk: str, mask: list=None) -> dict:
#     return {
#         "str": svyabk,
#         "object": svyabk_to_dict(svyabk),
#         "wildcard": svyabk_to_dict(svyabk_wildcards(svyabk, mask))
#     }
