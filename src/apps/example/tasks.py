from datetime import datetime

import dramatiq


@dramatiq.actor
def print_current_date():
    print(datetime.now())
