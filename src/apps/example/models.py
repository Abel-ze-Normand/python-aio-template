from collections import OrderedDict
from .sql_queries import ExampleSpec
from common import serializators
from common.base_dao import BaseDAO


class ExampleDAO(BaseDAO):
    spec_cls = ExampleSpec
    _fields = OrderedDict((
        ('example_field', serializators.as_is),
    ))
