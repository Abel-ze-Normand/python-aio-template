from collections import OrderedDict

from sqlbuilder.smartsql import Constant, func

from common.spec import Spec
from common.sqlbuilder_common import T, Q, Cast


class ExampleSpec(Spec):
    __mytable_t = T.mytable
    _fields = OrderedDict((
        ('field1', __mytable_t.field1),
    ))

    def __init__(self, *args, **kwargs):
        self.param1 = kwargs['param1']
        super().__init__(*args, **kwargs)

    def defer_fields_init(self):
        self._fields.update(OrderedDict((
            ('computable_field', Cast(self.__mytable_t.field1, 'text').op('||')(self.param1)),
        )))

    def ids_filter(self, ids_t):
        return (ids_t, self.__mytable_t.field1, 'int')

    def main_tables(self):
        return self.__mytable_t
