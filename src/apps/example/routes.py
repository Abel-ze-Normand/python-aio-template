from .views import ExampleView


routes = [
    ('GET', '/example/', ExampleView, 'get_example'),
]
