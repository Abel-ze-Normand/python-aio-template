from .models import ExampleDAO
from common.validators import BaseValidator, bin_validator


class ExampleValidator(BaseValidator):
    _extra_schema = {
        'param1': bin_validator('param1'),
    }
