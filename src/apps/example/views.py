from .models import ExampleDAO
from .validators import ExampleValidator
from common.base_view import BaseView


class ExampleView(BaseView):
    validator = ExampleValidator
    model = ExampleDAO
    kind = 'getExampleList'
    with_pagination = False
