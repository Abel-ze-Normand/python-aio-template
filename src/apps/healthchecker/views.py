from aiohttp.web import Response


async def healthcheck(request):
    return Response(text='OK')


async def index(request):
    return Response(text='e2-filters-api')
