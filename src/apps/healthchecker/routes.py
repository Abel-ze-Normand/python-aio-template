from . import views

routes = (
    ('GET', '/healthcheck', views.healthcheck, 'healthcheck'),
    ('GET', '/', views.index, 'index'),
)
