from collections import OrderedDict
from common.spec import ModifyingSpec, Spec, Param
from common.sqlbuilder_common import *
from sqlbuilder.smartsql.expressions import Array


class Spec1(ModifyingSpec):
    _target_tbl = T.test_tbl.as_('a')
    _fields = (_target_tbl.f1, _target_tbl.f2)

    def prepare_data_update(self, data):
        return data

    def apply_criteria(self, q):
        return q.where(self._target_tbl.f1 > 1)

print(Spec1({'id': 1, 'name': 'testname'}).update_q_str)


class Spec2(ModifyingSpec):
    _target_tbl = T.test_tbl.as_('a')
    _fields = (_target_tbl.f1, _target_tbl.f2)

    def prepare_data(self, data):
        return data

print(Spec2({'id': 2, 'name': 'test'}).save_q_str)


class Spec3(ModifyingSpec):
    _target_tbl = T.test_tbl.as_('a')
    _fields = (_target_tbl.f1, _target_tbl.f2)
    _returning = (_target_tbl.f1,)

    def prepare_data(self, data):
        return {
            'id': data['id'],
            'namearr': Array(data['namearr'])
        }

print(Spec3({'id': 2, 'namearr': [1, '2', 3]}).save_q_str)


# class Spec3(Spec):
#     _t = T.test_tbl.as_('a')
#     _fields = OrderedDict((
#         ('id', _t.f1),
#         ('name', _t.f2),
#     ))

#     def main_tables(self):
#         return self._t

#     def ids_filter(self, ids_t):
#         ids = ids_t.as_('id')
#         return (ids, self._t.f1, INTEGER)

#     def apply_criteria(self, q):
#         return q.where((self._t.f2 == 'bb') & (self._t.f2 == 'cc'))


# print(Spec3().collect_q_str)
