import pkgutil
from common.utils.logger import app_logger
from importlib import import_module
import apps


def setup_routes(app):
    for importer, modname, ispkg in pkgutil.iter_modules(apps.__path__):
        try:
            # ищем во всех приложениях модуль routes и если есть регистрируем
            routes = import_module(f'apps.{modname}.routes')
            register(app, routes.routes)
            app_logger.debug(f'Routes for {modname} has been registered.')
        except Exception as e:
            app_logger.warning(f'Routes for {modname} has not been registered.',
                               exc_info=True)


def register(app, routes: list):
    for method, path, handler, name in routes:
        app.router.add_route(method, path, handler, name=name)
