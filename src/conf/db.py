import logging
import ujson

import asyncpg

from . import settings


logger = logging.getLogger(__name__)


def ujson_encoder(value):
    # тут пробрасываем значение как есть, т.к. мы сами его кодируем
    return value


def ujson_decoder(value):
    return ujson.loads(value)


async def init_connection(connection):
    await connection.set_type_codec(
        'jsonb', encoder=ujson_encoder, decoder=ujson_decoder,
        schema='pg_catalog'
    )


async def setup_database(app):
    app.pool = await asyncpg.create_pool(dsn=settings.DATABASE_URL, init=init_connection,
                                         min_size=settings.DB_POOL_CONNECTIONS_MIN_SIZE,
                                         max_size=settings.DB_POOL_CONNECTIONS_MAX_SIZE)
