import sys

from envparse import env
from os.path import isfile
import logging
from schema import Or

IS_TEST_MODE = any('test' in arg for arg in sys.argv)

if isfile('.env'):
    env.read_envfile('.env')

ENV = env('RUN_ENV', '')

DEBUG = ENV != 'prod'

log_level_names = [
    logging.getLevelName(level)
    for level in (logging.CRITICAL,
                  logging.ERROR,
                  logging.WARNING,
                  logging.INFO,
                  logging.DEBUG,
                  logging.NOTSET)
]
error_log_level = logging.getLevelName(logging.ERROR)


def get_log_level_from_env(var_name, default):
    validator = Or(*log_level_names,
                   error=f'{var_name} failed. "{{}}" not in {log_level_names}')
    return validator.validate(env(var_name, default))


LOG_LEVEL = get_log_level_from_env('API_LOG_LEVEL', error_log_level)

SENTRY_DSN = env('API_SENTRY_DSN', '')
SENTRY_LEVEL = get_log_level_from_env('SENTRY_LEVEL', error_log_level)

API_VERSION = env('API_VERSION', "0.0.1")
SERVICE_NAME = env('SERVICE_NAME', 'example')

LISTEN_HOST = env('API_LISTEN_HOST', '0.0.0.0')
LISTEN_PORT = env.int('API_LISTEN_PORT', default=8057)

SELF_API_URL = env('SELF_API_URL', 'http://0.0.0.0:8057')

ROUTE_PREFIX = env('ROUTE_PREFIX', '').rstrip('/')

DATABASE_URL = env('DATABASE_URL', env('DSN', ''))
DB_POOL_CONNECTIONS_MIN_SIZE = env('DB_POOL_CONNECTIONS_MIN_SIZE', default=1, cast=int)
DB_POOL_CONNECTIONS_MAX_SIZE = env('DB_POOL_CONNECTIONS_MAX_SIZE', default=1, cast=int)

MIN_LIMIT = env('MIN_LIMIT', cast=int, default=20)
MIN_OFFSET = env('MIN_OFFSET', cast=int, default=0)

MAX_LEVEL = env('MAX_LEVEL', cast=int, default=99)

PERIODIC_TASKS = (
    #('crontab_format', 'module_path:called function'),
    ('* * * * *', 'apps.example.tasks:print_current_date.send'),
)
