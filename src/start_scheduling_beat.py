from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.cron import CronTrigger

from conf.settings import PERIODIC_TASKS

if __name__ == "__main__":
    scheduler = BlockingScheduler()
    print('List of registered periodic tasks:')
    for periodic_time, periodic_reference in PERIODIC_TASKS:
        scheduler.add_job(func=periodic_reference, trigger=CronTrigger.from_crontab(periodic_time))
        print(f" '{periodic_reference}'   on crontab '{periodic_time}'")
    try:
        scheduler.start()
    except KeyboardInterrupt:
        scheduler.shutdown()
