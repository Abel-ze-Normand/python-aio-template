import asyncio
import uvloop
from main import build_application


async def cons_app():
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    # loop = asyncio.get_event_loop()
    return await build_application()
