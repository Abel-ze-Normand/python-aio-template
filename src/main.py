import uvloop
import asyncio
from aiohttp.web import Application
from common.middlewares import get_middlewares
from common.utils.prometheus import setup_metrics
from conf.db import setup_database
from conf.routes import setup_routes
from conf import settings
from conf import lazy_init
from common.utils.logger import app_logger


async def build_application() -> Application:
    app = Application(
        middlewares=get_middlewares(),
    )
    await setup_database(app)

    await lazy_init.init_lazy_settings(app)
    setup_routes(app)

    if not settings.IS_TEST_MODE:
        setup_metrics(app, settings.SERVICE_NAME)

    return app


if __name__ == '__main__':
    asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
    loop = asyncio.get_event_loop()
    web_app = loop.run_until_complete(build_application())

    handler = web_app.make_handler()
    pre_srv = loop.create_server(handler, settings.LISTEN_HOST, settings.LISTEN_PORT)
    srv = loop.run_until_complete(pre_srv)

    app_logger.debug('serving on http://%s:%s', *srv.sockets[0].getsockname())
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        srv.close()
        loop.run_until_complete(srv.wait_closed())
        loop.run_until_complete(web_app.shutdown())
        loop.run_until_complete(handler.shutdown(60.0))
        loop.run_until_complete(web_app.cleanup())
    loop.close()
