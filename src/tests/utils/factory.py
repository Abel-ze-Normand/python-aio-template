class ValidatorTestData(object):
    @staticmethod
    def factory(validator_type: str):
        return {
            'by_hierarchy_node_request_validator': ByHierarchyNodeRequestValidatorTestData(),
        }[validator_type]


class ByHierarchyNodeRequestValidatorTestData(ValidatorTestData):
    def get_test_data(self):
        test_data = (
            ({'businessId': 'ENRG', 'ids': '1,3,5'},
             {'business_id': 'ENRG', 'ids': (1, 3, 5)}),
            ({'businessId': 'ENRG', 'ids': '1', 'itemsPerPage': '1', 'startIndex': '1'},
             {'business_id': 'ENRG', 'ids': (1,), 'limit': 1, 'offset': 1}),
            ({'businessId': 'ENRG', 'ids': '1,3', 'cityId': '12'},
             {'business_id': 'ENRG', 'ids': (1, 3), 'city_id': '12'}),
            ({'businessId': 'ENRG', 'ids': '2,8,4,1', 'departmentId': '2'},
             {'business_id': 'ENRG', 'ids': (2, 8, 4, 1), 'department_id': '2'}),
            ({'businessId': 'ENRG', 'ids': '9', 'priceType': 'type'},
             {'business_id': 'ENRG', 'ids': (9,), 'price_type': 'type'}),
            ({'businessId': 'ENRG', 'sortDir': 'asc', 'sortField': 'date', 'ids': '1,4'},
             {'business_id': 'ENRG', 'sort_dir': 'asc', 'sort_field': 'date', 'ids': (1, 4)}),
            ({'businessId': 'ENRG', 'ids': '1,3,5', 'aggregations': '0'},
             {'business_id': 'ENRG', 'ids': (1, 3, 5), 'aggregations': 0}),
        )
        return test_data
