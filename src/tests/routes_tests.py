from .test_api_call import handler_data_api_call
from .test_middleware_errors import handler_aiohttp_error, handler_custom_error, handler_custom_error_params, \
    handler_ok, handler_system_error, handle_many_error, handle_base_error, hadle_cancelled_error

routes = [
    # Test decorator api_call
    ('GET', '/get_data_api_call', handler_data_api_call, 'get_data_api_call'),

    # Test middleware log_exception
    ('GET', '/ok_middleware_error', handler_ok, 'ok_middleware_error'),
    ('GET', '/system_err_middleware_error', handler_system_error, 'system_err_middleware_error'),
    ('GET', '/aiohttp_err_middleware_error', handler_aiohttp_error, 'aiohttp_err_middleware_error'),
    ('GET', '/custom_err_middleware_error', handler_custom_error, 'custom_err_middleware_error'),
    ('GET', '/custom_err_param_middleware_error', handler_custom_error_params, 'custom_err_param_middleware_error'),
    ('GET', '/many_err_middleware_error', handle_many_error, 'many_err_middleware_error'),
    ('GET', '/base_err_middleware_error', handle_base_error, 'base_err_middleware_error'),
    ('GET', '/cancelled_err_middleware_error', hadle_cancelled_error, 'cancelled_err_middleware_error'),

]
