from schema import Schema


brand_response = Schema({
    "code": str,
    "name": str,
    "doc_count": int
})

value_response = Schema({
    "id": [int],
    "value": str,
    "doc_count": int
})

agg_option_response = Schema({
    "values": [value_response],
    "property_id": [str],
    "property_name": str,
    "property_usage_sort": int
})

agg_generic_product_response = Schema({
    "id": int,
    "name": str,
    "tags": list,
    "doc_count": int
})

aggregation_response = Schema({
    "brands": [brand_response],
    "options": [agg_option_response],
    "generic_products": [agg_generic_product_response]
})
