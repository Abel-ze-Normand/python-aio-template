from .product_item_response import product_item_response
from .common_response import (product_price_response,
                              image_response,
                              promo_response,
                              options_response,
                              filter_to_apply_response,
                              variable_property)

from schema import Schema, Optional, Or


product_response = Schema({
    Optional("id"): int,
    Optional("product_name"): str,
    Optional("price"): Or(product_price_response, dict),
    Optional("product_class"): str,
    Optional("images"): Or([image_response], list),
    Optional("available"): bool,
    Optional("promo"): [promo_response],
    Optional("description"): str,
    Optional("default_unit"): str,
    Optional("options"): [options_response],
    Optional("product_items"): Or([product_item_response], list),
    Optional("filters_to_apply"): Or([filter_to_apply_response], list),
    Optional("variable_properties"): [variable_property],
    Optional("product_group_properties"): [str]
})
