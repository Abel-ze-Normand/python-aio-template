from schema import Schema, Optional, Or


unit_response = Schema({
    "id": str,
    "name": str,
    "weight": str,
    "volume": str
})

property_response = Schema({
    "name": str,
    "value": str
})

price_response = Schema({
    "unit_id": str,
    "price": str,
    "currency_id": str,
    "currency_short_name": str
})

image_response = Schema({
    "sort": int,
    "urls": dict
})

option_response = Schema({
    "property_id": int,
    "value_id": int,
    "property_name": str,
    "property_usage_sort": int,
    "property_tags": list,
    "value_tags": list,
    "value": str
})

options_response = Schema({
    "keys_options": Or([option_response], list),
    "facets_options": Or([option_response], list),
    "card_options": Or([option_response], list)
})

promo_response = Schema({
    "name": str,
    "value": str
})

generic_product_response = Schema({
    "id": int,
    "name": str,
    "tags": list
})

product_price_response = Schema({
    "unit_id": str,
    "max_price": float,
    "min_price": float,
    "currency_id": str,
    "currency_short_name": str
})

filter_to_apply_response = Schema({
    "value": str,
    "attribute": str
})

variable_property = Schema({
    "attr": str,
    "meta_attr": str,
    "prop_name": str
})

search_dropdown_response = Schema({
    Optional("id"): int,
    Optional("name"): str,
    Optional("doc_count"): int
})

product_mappings_response = Schema({
    Optional("article_number"): str,
    Optional("product_item_id"): str
})

hierarchy_response = Schema({
    Optional("id"): Or(int, None),
    Optional("parent_id"): int,
    Optional("name"): str,
    Optional("level"): int,
    Optional("uri_name"): str,
    Optional("sort_in_level"): int,
    Optional("is_leaf_category"): bool,
    Optional("leaf_generic_product_ids"): Or([int], list),
    Optional("generic_product_ids"): Or([int], list)
})

generic_product_property_value_response = Schema({
    Optional("id"): int,
    Optional("name"): str,
    Optional("altname"): str,
    Optional("terms"): list,
    Optional("stm"): bool,
    Optional("deleted"): bool,
    Optional("display"): bool,
    Optional("property_id"): int,
    Optional("generic_product_property_id"): int,
    Optional("generic_product_id"): int
})
