from datetime import datetime

from schema import Schema, Optional


usage_response = Schema({
    "id": str,
    "deleted": bool,
    "possort": int
})

gp_value_response = Schema({
    "id": int,
    "stm": bool,
    "name": str,
    "terms": [str],
    "altname": str,
    "deleted": bool,
    "display": bool
})

gp_property_response = Schema({
    "id": int,
    "name": str,
    "terms": list,
    "prefix": str,
    "usages": [usage_response],
    "values": [gp_value_response],
    "deleted": bool,
    "multval": bool,
    "postfix": str,
    "valflag": bool,
    "gmprop_id": int
})

generic_product_response = Schema({
    Optional("id"): int,
    Optional("name"): str,
    Optional("properties"): [gp_property_response],
    Optional("deleted"): bool,
    Optional("created_at"): datetime,
    Optional("updated_at"): datetime
})

gp_property_product_response = Schema({
    "generic_product_id": int,
    "property_id": int,
}.update(gp_property_response.__dict__))
