from schema import Schema, Optional, Or
from .aggregation_response import aggregation_response


def get_api_schema(items_schema: Schema = None) -> Schema:
    data = Schema({
        "kind": str,
        Optional("fields"): str,
        Optional("currentItemCount"): int,
        Optional("itemsPerPage"): int,
        Optional("startIndex"): int,
        Optional("nextLink"): str,
        Optional("totalItems"): int,
        Optional("aggregations"): Or(aggregation_response, dict),
        "items": Or([items_schema], list)
    })

    api_schema = Schema({
        "apiVersion": str,
        Optional("context"): str,
        Optional("id"): str,
        Optional("method"): str,
        "data": data
    })

    return api_schema
