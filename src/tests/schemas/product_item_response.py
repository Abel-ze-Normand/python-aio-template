from .common_response import (unit_response,
                              price_response,
                              image_response,
                              options_response,
                              promo_response,
                              generic_product_response)

from schema import Schema, Optional, Or

product_item_response = Schema({
    Optional("id"): str,
    Optional("name"): str,
    Optional("units"): [unit_response],
    Optional("default_unit"): str,
    Optional("prices"): Or([price_response], list),
    Optional("available"): bool,
    Optional("images"): Or([image_response], list),
    Optional("promo"): [promo_response],
    Optional("options"): [options_response],
    Optional("generic_product"): generic_product_response
})
