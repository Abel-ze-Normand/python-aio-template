import asyncio
import pytest
import uvloop
from .tests.routes_tests import routes
from main import build_application


@pytest.fixture
def loop():
    loop = uvloop.new_event_loop()
    asyncio.set_event_loop(loop)
    yield loop
    loop.close()


@pytest.fixture
def app(loop):
    _app = build_application(loop)
    yield _app


@pytest.fixture
def app_server(loop, app, test_server):
    return loop.run_until_complete(test_server(app))


@pytest.fixture
def app_client(loop, app, test_client):
    return loop.run_until_complete(test_client(app))


def register(app, routes):
    for method, path, handler, name in routes:
        app.router.add_route(method, path, handler, name=name)


@pytest.fixture
def catalog_server(loop, test_client):
    app = build_application(loop)
    register(app, routes)
    return loop.run_until_complete(test_client(app))
